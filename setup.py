"""setup.py for the jetbot application"""

import os
from setuptools import setup, find_packages

with open("requirements/drive-requirements.in") as f:
    INSTALL_REQUIRES_LIST = f.read().splitlines()

if "JETAPP_NOCV" in os.environ:
    INSTALL_REQUIRES_LIST.remove("opencv-python")

setup(
    name="JetbotApp",
    version="0.0.0",
    packages=find_packages(exclude=("tests", "tests.*")),
    package_dir={"jetapp": "jetapp"},
    package_data={"jetapp": ["data/*"]},
    data_files=[],
    zip_safe=False,
    install_requires=INSTALL_REQUIRES_LIST,
    entry_points={
        "console_scripts": [
            "jetapp-train = jetapp.train:main",
            "jetapp-capture = jetapp.capture:main",
            "jetapp-drive = jetapp.drive:main",
        ]
    },
)
