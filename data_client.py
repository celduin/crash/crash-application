"""script for displaying the jetbots video stream"""

import atexit
import queue
import sys
import time

import cv2

from jetapp.camera import UdpSrc
from jetapp.joystick import MouseJoystick, PS3Joystick, XboxJoystick
from jetapp.utils import MQConnection, Circle, get_cli_configs


class JetbotDataStreamClient:
    """ Consumes video and labelling data from the Robot and displays
        in an opencv window
    """

    def __init__(self, host="localhost", camera=False):
        self.local_queue = queue.Queue()
        self.mq_conn = MQConnection(
            host,
            user="client",
            password="client",
            queue_name="jetbot",
            local_queue=self.local_queue,
        )
        self.label_data = None
        if camera:
            self.camera_stream = UdpSrc()
        else:
            self.camera_stream = None
        atexit.register(self.release)

    def tick(self):
        """ Polls the data queue and gets the latest frame from the rtp stream
        """
        # get latest image from camera
        frame = self.camera_stream.get_frame()
        if frame is None:
            sys.stderr.write("Error: no data from camera\n")
            return False
        # Poll local_queue for latest labelling data
        # TODO: rethink this, currently just strips queue and ignores all
        #       but latest
        while not self.local_queue.empty():
            try:
                self.label_data = self.local_queue.get_nowait()
            except queue.Empty:
                break

        # if we get a circle, draw it on the frame
        new_circle = Circle.from_string(self.label_data)
        print("Got circle:", new_circle)

        width, height = self.camera_stream.get_dimensions()

        if new_circle is None:
            sys.stderr.write("Warning: invalid circle data found\n")
        else:
            # update circle from normalised image space to pixel space
            width, height = self.camera_stream.get_dimensions()
            coord_x = int((width / 2) * (new_circle.center[0] + 1))
            coord_y = int((height / 2) * (new_circle.center[1] + 1))
            new_circle.center = (coord_x, coord_y)
            # draw onto frame
            new_circle.draw(frame)

        cv2.imshow("nanocam", frame)
        cv2.waitKey(1)
        return True

    def start(self):
        """Starts the connection to the jetbot"""
        self.mq_conn.start_consume()

    def stop(self):
        """Stops the connection to the jetbot"""
        self.mq_conn.stop_consume()

    def release(self):
        """Called as the python program stops, and properly cleans up the
        registers"""
        self.camera_stream.release()
        self.mq_conn.release()
        cv2.destroyAllWindows()


class JetbotPilotClient(JetbotDataStreamClient):
    """ Allows manual driving of jetbot and displays feed in an opencv window
    """

    def __init__(self, host="localhost", joystick=None):
        super().__init__(host=host)
        if joystick is None:
            joystick = MouseJoystick()
        self.joystick = joystick
        self.current_joystick_state = None

    def tick(self):
        """ Polls the data queue and gets the latest frame from the rtp stream
        """
        # Get latest image from camera
        if self.camera_stream:
            frame = self.camera_stream.get_frame()
        else:
            frame = None
        if frame is None:
            sys.stderr.write("Error: no data from camera\n")

        # Poll joystick for commands
        self.joystick.tick()

        js_state = self.joystick.get_state()

        if self.current_joystick_state != js_state:
            self.current_joystick_state = js_state
            print("Sending:", self.current_joystick_state)
            # Put joystick state on the MQ
            self.mq_conn.send(self.joystick.get_state().to_string())

        if frame:
            cv2.imshow("nanocam", frame)
            cv2.waitKey(1)
        return True

    def start(self):
        pass

    def stop(self):
        pass

    def release(self):
        self.mq_conn.release()
        cv2.destroyAllWindows()
        self.camera_stream.release()


def cli_add_arguments(parser, defaults):
    """Add arguments to the command line, to modify the connection
    :param parser:  contains the previous arguments and used for adding
                    arguments onto
    :type parser:   argparse.ArgumentParser
    :param defaults:contains the information for the IP address and visuals
    :type defaults: dict"""
    parser.add_argument(
        "--host",
        dest="host",
        help="Host to stream data to",
        type=str,
        default=defaults.get("host", "not-defined"),
        nargs="?",
    )

    parser.add_argument(
        "--mode",
        dest="mode",
        help="Mode to launch client in [auto|pilot]",
        type=str,
        default=defaults.get("mode", "auto"),
        nargs="?",
    )

    parser.add_argument(
        "--control",
        dest="control",
        help="Type of controller to use for manual drive",
        type=str,
        default=defaults.get("control", "mouse"),
        nargs="?",
    )


def main():
    """ Sets up a connection to jetbot via localhost and attepmts to run it"""
    defaults = {"host": "localhost", "mode": "auto", "control": "mouse"}
    configs = get_cli_configs(
        "Car data client",
        "Reads rtp video stream and MQ data and overlays",
        defaults,
        cli_add_arguments,
    )

    if configs["mode"] == "auto":
        client = JetbotDataStreamClient(configs["host"])
    elif configs["mode"] == "pilot":
        if configs["control"] == "mouse":
            joystick = MouseJoystick()
        elif configs["control"] == "ps3":
            joystick = PS3Joystick()
        elif configs["control"] == "xbox":
            joystick = XboxJoystick()
        client = JetbotPilotClient(configs["host"], joystick=joystick)

    client.start()

    try:
        while True:
            if not client.tick():
                break
            # TODO: rate limit this to match frame rate / pred rate?
            time.sleep(0.1)
    except (KeyboardInterrupt, SystemExit):
        print("Exiting")
    finally:
        cv2.destroyAllWindows()
        client.stop()
        client.release()


if __name__ == "__main__":
    main()
