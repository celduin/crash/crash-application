"""This module is used for processing images so they can be used with the path
models"""

from enum import Enum
import os

import cv2
import numpy as np
import torch
import torchvision

from jetapp.utils import get_torch_device, BadImageException


def new_drive_model(pretrained=False, device=None):
    """ Generates a new pytorch module with the architecture used for our
        steering model.

        :param pretrained: Whether to use a pretrained model from ImageNet
        :type pretrained: bool

        :param device: The torch device the model is for
        :type device: torch.device

        :returns: A new pytorch module with steering architecture
        :rtype: torch.nn.Module
    """
    if device is None:
        device = get_torch_device()

    model = torchvision.models.resnet18(pretrained=pretrained)
    model.fc = torch.nn.Linear(512, 1)
    model.to(device)
    return model


def new_avoid_model(pretrained=False, device=None, categories=2):
    """ Generates a new pytorch module with the architecture used for our
        object detection model.

        :param pretrained: Whether to use a pretrained model from ImageNet
        :type pretrained: bool

        :param device: The torch device the model is for
        :type device: torch.device

        :param categories: The number of categories the model will be able
        to classify.
        :type categories: int

        :returns: A new pytorch module with object detection architecture
        :rtype: torch.nn.Module
    """
    if device is None:
        device = get_torch_device()

    model = torchvision.models.alexnet(pretrained=pretrained)
    model.classifier[6] = torch.nn.Linear(
        model.classifier[6].in_features, categories
    )
    model.to(device)
    return model


class ModelsWrapper:
    """A convenience class to hold the two models loaded into pytorch, to make
    things more convenient when actually processing"""

    def __init__(
        self,
        steering="best_steering_model_xy.pth",
        avoid="best_model.pth",
        count_cat=2,
    ):
        self.model_drive = new_drive_model(pretrained=False).eval()
        self.model_drive.load_state_dict(torch.load(steering))

        self.model_avoid = new_avoid_model(
            pretrained=False, categories=count_cat
        )
        self.model_avoid.load_state_dict(torch.load(avoid))


class PreProcessor:
    """ Preprocess the image or camera data to be used with torch and
        torchvision
    """

    # TODO: document these magic numbers, or make them variables.
    #       what drives them???
    mean_data = [0.485, 0.456, 0.406]
    stddev_data = [0.229, 0.224, 0.225]

    class Mode(Enum):
        """ Enum for what mode to use the preprocessor in.

            This is needed as some bits of preprocessing are not suitable for
            both training and evaluating.
        """

        EVAL = 0
        """ Evaluation Mode"""

        TRAIN = 1
        """ Training Mode"""

    def __init__(self, mode=Mode.EVAL):
        self.device = get_torch_device()
        self.mean_drive = np.array(self.mean_data)
        self.std_drive = np.array(self.stddev_data)
        self.normalize_drive = torchvision.transforms.Normalize(
            self.mean_drive, self.std_drive
        )
        self.mean_av = 255.0 * np.array(self.mean_data)
        self.std_av = 255.0 * np.array(self.stddev_data)
        self.normalize_avoid = torchvision.transforms.Normalize(
            self.mean_av, self.std_av
        )
        self.mode = mode

    def preprocess(self, image, hflip=False):
        """ Preprocesses the image to be used for the steering model

            :param image:   the image that needs preprocessing
            :type image:    numpy.array

            :raises BadImageException:

            :param hflip:   weather to randomly horizontally flip
            :type hflip:    bool

            :returns:       the image in a uesful format
            :rtype:         torch.Tensor
        """

        # Convert the BGR image to RGB colour space
        #
        image = cv2.resize(image, (244, 244))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Check whether the frame is an unusable black image
        #
        blackcheck_pixels_per_pixel_checked = 20
        blackcheck_minimum_average_colour = 80
        colourav = np.mean(
            image[
                122::blackcheck_pixels_per_pixel_checked,
                ::blackcheck_pixels_per_pixel_checked,
                :,
            ],
            axis=2,
        )
        if (np.mean(colourav[1]) < blackcheck_minimum_average_colour) and (
            np.mean(colourav[2]) < blackcheck_minimum_average_colour
        ):
            raise BadImageException("""Stopping: Image too dark""")

        # Blank out top half of image
        # TODO: Make this less of a hack
        #
        image[:122, :, :] = 0

        if hflip:
            image = image[:, ::-1, :]

        # If we're training, apply a random change to brightness, contrast,
        # saturation and hue
        #
        if self.mode == PreProcessor.Mode.TRAIN:
            image = torchvision.transforms.functional.to_pil_image(image)
            image = torchvision.transforms.ColorJitter(0.3, 0.3, 0.3, 0.3)(
                image
            )

        # Normalize the image colours
        #
        image = torchvision.transforms.functional.to_tensor(image)
        image = self.normalize_drive(image)

        # This doesn't work when being run in a fork, which is quietly done by
        # the data loader in training.
        #
        # We also take the opportunity to increase the dimension of the image
        # by 1, as the model expects an array of images
        #
        if self.mode == PreProcessor.Mode.EVAL:
            image = image.to(self.device).float()[None, ...]

        return image

    def preprocess_av(self, camera_value):
        """ Preprocesses the image to be used for the avoidance model

            :param camera_value:the camera readings that needs preprocessing
            :type camera_value: numpy.array

            :returns:   the camera value in a uesful format
            :rtype:     torch.Tensor
        """

        image = cv2.cvtColor(camera_value, cv2.COLOR_BGR2RGB)
        image = image.transpose((2, 0, 1))
        image = torch.from_numpy(image).float()
        image = self.normalize_avoid(image)

        if self.mode == PreProcessor.Mode.EVAL:
            image = image.to(self.device).float()[None, ...]

        return image


def parse_filename(filename):
    """ Parses frame filename and extracts turning radius reciprocal and
        forward velocity. Filename format:

        img_{img_id}_R_{R_max/R}~{1/R}_V_{V/V_max}~{V}.jpg

        :param filename:    frame filename
        :type filename:     str

        :returns:           Tuple of radius reciprocal, velocity
        :rtype:             (float, float)
    """
    file_split = os.path.basename(os.path.splitext(filename)[0]).split("_")
    radius_recip = float(file_split[3].split("~")[0])
    velocity = float(file_split[5].split("~")[0])
    return (radius_recip, velocity)
