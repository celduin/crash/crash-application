"""Module for controlling the jetbot"""
import json
import os

from Adafruit_MotorHAT import Adafruit_MotorHAT
import numpy.polynomial.polynomial as poly

from jetapp.utils import sign
from .motor import Motor


class MotorMap:
    """ Simple wrapper around motor map file. Performs basic interpolation.
    """

    def __init__(self, filename):
        """ Constructor

            :param filename: json motor map file to parse
            :type filename:  str
        """
        with open(filename, "r") as map_file:
            data = json.load(map_file)
        self.data = {float(k): data[k] for k in data.keys()}
        print("self.data", self.data)
        self.ctrl, self.cm_s = zip(
            *sorted(self.data.items(), key=lambda el: el[0])
        )

        self.cm_s_max = 0
        self.beta = None
        for ctrl, cm_s in zip(self.ctrl, self.cm_s):
            if (cm_s > 0) and (self.beta is None):
                self.beta = ctrl
            if cm_s > self.cm_s_max:
                self.cm_s_max = cm_s

        positive_controls = [x for x in self.ctrl if x >= self.beta]
        positive_speeds = [self.data[k] for k in positive_controls]

        # TODO: Use a less hacky way to convert speed - motor
        # We generate polynomials to approximate the motor map, however
        # thanks to the nature of the motor, at the moment these maps are
        # tricky on the domain/codomain.
        #
        # The below functions use a range tuned to be correct for the current
        # motor map.
        self.to_speed_coefs = poly.polyfit(
            positive_controls, positive_speeds, 5
        )
        self.to_motor_coefs = poly.polyfit(self.cm_s, self.ctrl, 5)

    def convert_speed(self, motor_cmd):
        """ Map the normalised motor command onto cm/s. Basic interpolation.

            This should be roughly a function [0, 1] -> [0, 20]

            :param motor_cmd: normalised motor cmd
            :type motor_cmd: float

            :returns: speed in cm/s
            :rtype: float
        """
        direction = sign(motor_cmd)
        motor_cmd = abs(motor_cmd)
        if motor_cmd < self.beta:
            return 0
        if motor_cmd > 1:
            return self.cm_s_max
        return direction * poly.polyval(motor_cmd, self.to_speed_coefs)

    def convert_motor(self, speed):
        """ Map the speed in cm/s onto a normalised motor command.
            (Almost) Inverse of convert_speed.

            This should be roughly a function [0, 20] -> [0, 1]

            :param speed: Speed of the robot in cm/s
            :type speed: float

            :returns: normalised motor cmd
            :rtype: float
        """
        direction = sign(speed)
        speed = abs(speed)
        if speed == 0:
            return 0
        if speed > self.cm_s_max:
            return 1
        return direction * poly.polyval(speed, self.to_motor_coefs)

    def get_max_speed(self):
        """ Gets the max speed form the motor map

            :returns: The max speed
            :rtype:   float
        """
        return self.cm_s_max


class Robot:
    """ This class abstracts the controlling of a jetbot.

        We drive the jetbot by calculating the reciprocal of the turn radius
        it needs to go through, and set the speed accordingly.
    """

    # config
    i2c_bus = 1
    left_motor_channel = 1
    left_motor_alpha = 1.0
    right_motor_channel = 2
    right_motor_alpha = 1.0

    # robot properties
    wheel_base = 10.2  # cm
    """ Distance between the wheels
    """

    wheel_diam = 6  # cm
    """ Diameter of the wheels
    """

    driver_initialiser = Adafruit_MotorHAT

    def __init__(self, motor_file=None, motor_max=0.3):
        """ Constructor

            :param motor_file: File containing the motor map
            :type motor_file:  str

            :param motor_max:  Maximum (average) voltage to supply to motors
            :type motor_max:   float
        """
        # TODO: add config option here
        self.motor_driver = self.driver_initialiser(i2c_bus=self.i2c_bus)
        self.left_motor = Motor(
            self.motor_driver,
            channel=self.left_motor_channel,
            alpha=self.left_motor_alpha,
        )
        self.right_motor = Motor(
            self.motor_driver,
            channel=self.right_motor_channel,
            alpha=self.right_motor_alpha,
        )

        # read map and set conversion function
        if motor_file is None:
            motor_file = os.path.join(
                os.path.dirname(__file__), "..", "data", "motor_map.json"
            )

        # Characterise our motor
        self.motor_map = MotorMap(motor_file)
        self.motor_max = motor_max
        self.speed_max = self.motor_map.convert_speed(self.motor_max)

        # Set our speed to 0, direction straight
        self.speed_avg = 0
        self.speed_left = 0
        self.speed_right = 0
        self.turn_rad_recip = 0

    def set_speed(self, speed_left, speed_right):
        """ Set the speeds of each wheel of the bot, and calculate the
        reciprocal of the turn radius

        :param speed_left: speed of left wheel in cm/s
        :type speed_left:  float

        :param speed_right: speed of right wheel in cm/s
        :type speed_right:  float
        """
        self.speed_left = speed_left
        self.speed_right = speed_right
        self.turn_rad_recip = self.speeds_to_rad_recip(speed_left, speed_right)
        self.set_motors(speed_left, speed_right)

    def set_turn_radius_recip(self, turn_rad_recip):
        """ Set the speeds required to make a turn of radius 1 / turn_rad_recip

        :param turn_rad_recip: reciprocal of turning radius
        :type turn_rad_recip:  float
        """
        self.turn_rad_recip = turn_rad_recip
        self.speed_left, self.speed_right = self.rad_recip_to_speeds(
            turn_rad_recip
        )
        self.set_motors(self.speed_left, self.speed_right)

    def rad_recip_to_speeds(self, rad_recip):
        """ Converts the Radius Reciprocal to speeds for each wheel.

            We do this by returning equal max speed for a straight line,
            otherwise we calculate speeds by:

                v_L = v + v_delta
                v_R = v - v_delta

            where v_delta is the difference in speed from the average speed.

            We calculate delta as follows:

                ^ |--..__
                | |----^ --..__
                | |    |----^  --..__
                | |    |    |        --..__
            v_L | |  v |    |              --..__
                | |    |    | v_R                --..__
                | |    |    |                          --..__
                | |    |    |                          a     --..__
                v |_______________________________________________==-
                                       R
                  <--------->
                       W

            This series of similar triangles is formed by the turning radius R,
            and the average, left and right speeds as the perpendiculars. It
            can be reasoned that angle a is the turning rate of the car, and
            that we have the following relations:

                v_L = (R + (W / 2))a
                v_R = (R - (W / 2))a
                v = v_L + v_delta
                v = v_R - v_delta

            Rearranging this system of equations yields:

                            v * W
                v_delta = -----------
                            R * 2

            :param rad_recip: The radius reciprocal to convert
            :type rad_recip:  float

            :returns:         Tuple of wheel speeds
            :rtype:           (float, float)
        """
        delta = self.wheel_base * self.speed_avg * rad_recip / 2
        return (self.speed_avg + delta, self.speed_avg - delta)

    def speeds_to_rad_recip(self, speed_left, speed_right):
        """ Converts the speeds of the two wheels to the reciprocal of the
            current turning radius

            We do this by calculating:

                1    2 * (v_L - v_R)
                - = -----------------
                R    W * (v_L + v_R)

            This is simply obtained by using the above system of equations and
            rearranging.

            :param speed_left: Speed of the left wheel
            :type speed_left:  float

            :param speed_right: Speed of the right wheel
            :type speed_right:  float

            :returns:           The reciprocal of the turn radius
            :rtype:             float
        """
        return (
            2
            * (speed_left - speed_right)
            / (self.wheel_base * (speed_left + speed_right))
        )

    def stop(self):
        """ Fully stops the motors and sets the current speeds to 0
        """
        self.speed_avg = 0
        self.speed_left = 0
        self.speed_right = 0
        self.left_motor.set_value(0)
        self.right_motor.set_value(0)

    def set_motors(self, left_speed, right_speed):
        """ Sets the left and right motors to achieve real speeds left_speed,
            right_speed.

            :param left_speed: The velocity the left wheel will provide
            :type left_speed:  float

            :param right_speed: The velocity the right wheel will provide
            :type right_speed:  float
        """
        left_speed = self.motor_map.convert_motor(left_speed)
        right_speed = self.motor_map.convert_motor(right_speed)
        self.left_motor.set_value(left_speed)
        self.right_motor.set_value(right_speed)
