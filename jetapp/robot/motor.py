"""Module for controlling the motor"""

import atexit
from Adafruit_MotorHAT import Adafruit_MotorHAT


class Motor:
    """API to control the motors"""

    def __init__(self, driver, channel, alpha=1.0, beta=0.0):
        """ Interface to AdaFruit driver.

        alpha and beta deternine the mapping:
            alpha=1 & beta=0 means 0 -> 0 and 1 -> 255

        :param driver:  Adafruit driver
        :type driver:   Class

        :param channel: which channel the motor is physically connected to
        :type channel:  int

        :param alpha:   determines linear mapping
        :type alpha:    float

        :param beta:    allows offset
        :type beta:     float
        """
        self.driver = driver
        self.motor = self.driver.getMotor(channel)
        self.channel = channel
        self.alpha = alpha
        self.beta = beta
        self.value = 0.0
        atexit.register(self._release)

    def set_value(self, value):
        """Sets motor value between [-1.0, 1.0]
        :param value:   value between -1.0 and 1.0 to make the motorl turn
                        clockwise or anticlockwise
        :type value:    float"""
        self.value = value
        mapped_value = int(255.0 * (self.alpha * value + self.beta))
        speed = min(max(abs(mapped_value), 0), 255)
        self.motor.setSpeed(speed)
        if mapped_value < 0:
            self.motor.run(Adafruit_MotorHAT.FORWARD)
        else:
            self.motor.run(Adafruit_MotorHAT.BACKWARD)

    def _release(self):
        """Stops motor by releasing control on python interpreter termination
        """
        self.motor.run(Adafruit_MotorHAT.RELEASE)
