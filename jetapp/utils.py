"""This module has utilitios such as being able to create and get circles, and
connecting to the jetbot over Wi-Fi and creating threads"""

import argparse
import atexit
import json
import queue
import os
import sys
import threading

import cv2
import pika
from torch import device as torch_device
from torch.cuda import is_available as cuda_available


def sign(num):
    """ Returns the sign of number num.

        :param num: The number to be processed
        :type num:  float

        :returns:   The sign of num
        :rtype:     int
    """
    return (-1, 1)[num >= 0]


def ensure_data_dir_exists(data_dir):
    """ Ensures that the specified directory exists

        :param data_dir: The directory to check
        :type data_dir:  str
    """
    try:
        os.makedirs(data_dir)
    except FileExistsError:
        pass


class LockableStore:
    """ Object with some of same interface as queue.Queue, containing one
        object that's updatable
    """

    def __init__(self):
        self.item = None
        self.lock = threading.RLock()

    def put(self, item):
        """ Set the store's object to item

            :param item: The item to be stored
            :type item:  any
        """
        with self.lock:
            self.item = item

    def get(self):
        """ Get the item currently stored

            :returns: The current stored item
            :rtype:   any
        """
        if self.item is None:
            raise queue.Empty
        return self.item

    def empty(self):
        """ Checks if the store is empty

            :returns: Whether the store is empty
            :rtype:   bool
        """
        return self.item is None


class MQConnection:
    """ Wraps a pika / rabbitmq connection for sending data to client
    """

    class ConsumeThread(threading.Thread):
        """ Stoppable MQ thread
        """

        def __init__(self, local_queue, connection):
            # TODO: make all the following configurable
            threading.Thread.__init__(self)
            self.mq_conn = connection
            self.stop_event = threading.Event()
            self.local_queue = local_queue

        def run(self):
            """ Polls the schedule and updates all the pipelines (sources)
            """
            for msg in self.mq_conn.channel.consume(
                self.mq_conn.queue_name, inactivity_timeout=1
            ):
                if self.stop_event.is_set():
                    break
                if not msg:
                    continue
                _, _, body = msg
                if body is None:
                    continue
                # TODO: see if we need to use method / props data
                if self.mq_conn.debug:
                    print("Received {}".format(body))
                self.local_queue.put(body)
            self.mq_conn.channel.cancel()

        def stop(self):
            """Stops the conumption of threads"""
            self.stop_event.set()

    def __init__(
        self,
        host,
        user="nano",
        password="nano",
        queue_name="test",
        port=5672,
        local_queue=None,
        debug=False,
    ):
        creds = pika.PlainCredentials(user, password)
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host, port, "/", creds)
        )
        self.channel = self.connection.channel()
        # ensure queue exists and clear it if it does
        self.queue_name = queue_name
        self.channel.queue_declare(queue=queue_name)
        self.channel.queue_purge(queue=queue_name)

        # initialise the consumption thread
        if local_queue is not None:
            self.consume_thread = MQConnection.ConsumeThread(local_queue, self)
        else:
            self.consume_thread = None

        self.debug = debug
        atexit.register(self.release)

    def send(self, body):
        """Sends what's in body to queue_name
        :param body:    what you want to send to the channel
        :type body:     str"""
        self.channel.basic_publish(
            exchange="", routing_key=self.queue_name, body=body
        )

    def reset_consume_thread(self, local_queue):
        """Stops communications and resets the thread
        :param local_queue: the queue on the local machine
        :type local_queue:  queue.Queue"""
        self.stop_consume(True)
        self.consume_thread = MQConnection.ConsumeThread(local_queue, self)

    def start_consume(self):
        """Starts consuming threads, if self.consume_threads isn't empty"""
        if self.consume_thread is None:
            sys.stderr.write("No consume thread\n")
            return
        self.consume_thread.start()

    def stop_consume(self, silent=False):
        """Stops the consumption of threads, and joins the currently conusmed
        threads
        :param silent:  if you want to silent the error messages or not
        :type silent:   bool
        """
        if (self.consume_thread is None) or (
            not self.consume_thread.is_alive()
        ):
            if not silent:
                sys.stderr.write("No consume thread running\n")
            return
        self.consume_thread.stop()
        self.consume_thread.join()

    def release(self):
        """Stop communications and close the connection"""
        self.stop_consume(True)
        if self.connection.is_open:
            self.connection.close()


class Serialisable:
    """ Abstract class for an object that is serialisable into JSON
    """

    def __eq__(self, other):
        if other is None:
            return False
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return "{}:{}".format(type(self).__name__, self.to_string())

    def to_string(self):
        """ Returns a serialised version of the object

            :returns: The object, serialised to a string
            :rtype:   str
        """
        return json.dumps(self.__dict__)


class Circle(Serialisable):
    """ Allows for serialising of circle and overlay onto opencv frame
    """

    COLOR_GREEN = (0, 255, 0)

    def __init__(self, center, radius, color=COLOR_GREEN, thickness=1):
        self.center = center
        self.radius = radius
        self.color = color
        self.thickness = thickness

    @classmethod
    def from_string(cls, obj_str):
        """A class method which creates a new Circle instance given a string
        in a json like format
        Returns the values for the circle based on the string format, if
        it's correct
        :param cls:     the Cirle class
        :type cls:      Circle

        :param obj_str: the definiton of the circle, as a string
        :type obj_str:  str

        :returns:       a Circle, based on information from obj_str
        :rtype:         Circle
        """
        if obj_str is None:
            return None
        try:
            data = json.loads(obj_str)
            out = cls(
                tuple(data["center"]),
                data["radius"],
                tuple(data["color"]),
                data["thickness"],
            )
        except (KeyError, json.JSONDecodeError):
            return None
        else:
            return out

    def draw(self, frame):
        """Draws a circle with the properties of center, radius, color, and
        thickness
        :param frame:   where the circle is drawn
        :type frame:    numpy.array"""
        cv2.circle(frame, self.center, self.radius, self.color, self.thickness)


def get_cli_configs(name, description, defaults, add_arguments=None):
    """ Common code for simple cli

    :param name:            main text in Usage string
    :type name:             str
    :param description:     description of cli
    :type description:      str
    :param defaults:        dict containing defaults
    :type defaults:         str
    :param add_arguments:   func allowing for calling code to add arguments
    :type add_arguments:    str

    :returns:               CLI config
    :rtype:                 dict

    """
    parser = argparse.ArgumentParser(name, description=description)

    if add_arguments is not None:
        add_arguments(parser, defaults)

    configs = vars(parser.parse_args())

    return configs


class JoystickState(Serialisable):
    """ Encapsulates the abstract controller state
    """

    def __init__(self, right_trigger=0, left_trigger=0, pad_left=0):
        self.right_trigger = right_trigger
        self.left_trigger = left_trigger
        self.pad_left = pad_left

    @classmethod
    def from_string(cls, obj_str):
        """ Create a new JoystickState from a string of JSON, provided it has
            the correct attributes

            :param obj_str: A string of JSON representing a JoystickState
            :type obj_str:  str

            :returns:       A new JoystickState generated from the JSON
            :rtype:         JoystickState
        """
        if obj_str is None:
            return None
        try:
            data = json.loads(obj_str)
            out = cls(
                data["right_trigger"], data["left_trigger"], data["pad_left"]
            )
        except (KeyError, json.JSONDecodeError):
            return None
        else:
            return out


def get_torch_device():
    """ Convenience function to return the torch device we want to use,
    that is CUDA if it's available, otherwise CPU.
    """
    if cuda_available():
        return torch_device("cuda")
    return torch_device("cpu")


class BadImageException(Exception):
    """ When an image which is mainly black and red is detected, the
    camera is deemed to be obstructed and so sending bad images.
    """
