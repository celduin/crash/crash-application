""" This module abstracts various joysticks to be used to manually drive the
    jetbot and collect training data.
"""
import time

import pygame

from jetapp.utils import JoystickState, sign


class PS3Joystick:
    """ A wrapper around pygame.joystick.Joystick to easier setup use of
        a PS3 controller to control the nano.

        Right trigger   - Move forwards
        Left trigger    - Move backwards
        Left thumbstick - Steering
    """

    RT_index = 5
    LT_index = 2
    LP_index = 0

    def __init__(self):
        self.state = JoystickState()
        pygame.init()

        found = False
        for index in range(pygame.joystick.get_count()):
            joystick = pygame.joystick.Joystick(index)
            joystick.init()
            print(joystick.get_name())
            if joystick.get_name() == "Sony PLAYSTATION(R)3 Controller":
                found = True
                self.joystick_index = index

        if not found:
            self.release()
            raise RuntimeError("No Sony Joystick found")

        # NB: this is to fix a bug with the controller driver (inits to 0]
        # before 1st input, as opposed to -1)
        #
        self.r_trigger_init = False
        self.l_trigger_init = False

        self.exponent = 1

    def update_state(self):
        """ Updates the JoystickState for the joystick based on the current
            inputs to the joystick.
        """
        joystick = pygame.joystick.Joystick(self.joystick_index)
        # right trigger
        if self.r_trigger_init:
            self.state.right_trigger = (
                (joystick.get_axis(self.RT_index) + 1) / 2
            ) ** self.exponent
        else:
            if joystick.get_axis(self.RT_index) != 0:
                self.r_trigger_init = True
        # left trigger
        if self.l_trigger_init:
            self.state.left_trigger = (
                (joystick.get_axis(2) + 1) / 2
            ) ** self.exponent
        else:
            if joystick.get_axis(self.LT_index) != 0:
                self.l_trigger_init = True
        # left pad
        raw_pad = joystick.get_axis(self.LP_index)
        self.state.pad_left = sign(raw_pad) * (abs(raw_pad) ** self.exponent)

    def tick(self):
        """ Wrapper around updating the state, that actually polls the pygame
            event queue.
        """
        # TODO: we have to poll the events, otherwise controller state doesn't
        #       get updated. See if we can use this information
        for _ in pygame.event.get():
            pass
        self.update_state()

    def get_state(self):
        """ Gives a copy of the internal joystick state.

            :returns: The current joystick state
            :rtype:   jetapp.utils.JoystickState
        """
        return JoystickState(
            self.state.right_trigger,
            self.state.left_trigger,
            self.state.pad_left,
        )

    def release(self):  # pylint: disable=no-self-use
        """ Release the controller
        """
        pygame.quit()


class MouseJoystick:
    """ A rather hacky way of getting the nano to be mouse controlled.
        This is rather difficult, and also opens a pygame window. To control
        use left click to move forwards, right to go backwards, and move the
        mouse left or right to turn.
    """

    # RT = left click, LT = right click
    rt_index = 0
    lt_index = 2

    def __init__(self):
        self.state = JoystickState()
        pygame.init()
        pygame.mouse.set_visible(True)

        self.screen_width = pygame.display.Info().current_w
        self.screen_height = pygame.display.Info().current_h

        pygame.display.set_mode((self.screen_width, self.screen_height))

        self.exponent = 2

    def update_state(self):
        """ Updates the JoystickState for the joystick based on the current
            inputs to the joystick.
        """

        mouse_buttons = pygame.mouse.get_pressed()
        if mouse_buttons[self.rt_index]:
            self.state.right_trigger = 1
        else:
            self.state.right_trigger = 0

        if mouse_buttons[self.lt_index]:
            self.state.left_trigger = 1
        else:
            self.state.left_trigger = 0

        mouse_pos = pygame.mouse.get_pos()[0]
        self.state.pad_left = self._scale_mouse_pos(mouse_pos)

    def _scale_mouse_pos(self, mouse_pos):
        """ Scales the mouse position to calibrate the turning.
            This should roughly correspond to the extreme edges of the screen
            being the size of the corner on the LEGO® track we use

            :param mouse_pos:   The raw mouse position
            :type mouse_pos:    int

            :returns:           The mouse position converted to be meaningful
            :rtype:             float
        """
        direction = sign(mouse_pos - (self.screen_width / 2))
        mouse_pos -= self.screen_width / 2
        mouse_pos /= self.screen_width / 2
        mouse_pos = direction * (abs(mouse_pos) ** self.exponent)
        mouse_pos /= 6
        return mouse_pos

    def tick(self):
        """ Wrapper around updating the state, that actually polls the pygame
            event queue.
        """
        for _ in pygame.event.get():
            pass
        self.update_state()

    def get_state(self):
        """ Gives a copy of the internal joystick state.

            :returns: The current joystick state
            :rtype:   jetapp.utils.JoystickState
        """
        return JoystickState(
            self.state.right_trigger,
            self.state.left_trigger,
            self.state.pad_left,
        )

    def release(self):  # pylint: disable=no-self-use
        """ Release the controller
        """
        pygame.quit()


class XboxJoystick:
    """ Wrapper around pygame.joystick; giving easy access to attached
        xbox controller.
    """

    RT_index = 5
    LT_index = 2
    LP_index = 0

    def __init__(self):
        # initialise state and pygame
        self.state = JoystickState()
        pygame.init()

        # detect if PS3 controller is attached
        found = False
        for index in range(pygame.joystick.get_count()):
            joystick = pygame.joystick.Joystick(index)
            joystick.init()
            print(joystick.get_name())
            if joystick.get_name() == "Microsoft X-Box 360 pad":
                found = True
                self.joystick_index = index

        if not found:
            self.release()
            raise RuntimeError("No X-Box Joystick found")

        # NB: this is to fix a bug with the controller driver (inits to 0
        #     before 1st input, as opposed to -1)
        self.r_trigger_init = False
        self.l_trigger_init = False

        # exponent determines the mapping from input -> output (i.e. this
        # will make the pad more sensitive towards the extremes, and less
        # sensitive around null)
        self.exponent = 2

    def update_state(self):
        """ Polls the pygame.joystick for current state and maps onto
            JoystickState object.
        """
        joystick = pygame.joystick.Joystick(self.joystick_index)
        # right trigger
        if self.r_trigger_init:
            self.state.right_trigger = (
                (joystick.get_axis(self.RT_index) + 1) / 2
            ) ** self.exponent
        else:
            if joystick.get_axis(self.RT_index) != 0:
                self.r_trigger_init = True
        # left trigger
        if self.l_trigger_init:
            self.state.left_trigger = (
                (joystick.get_axis(2) + 1) / 2
            ) ** self.exponent
        else:
            if joystick.get_axis(self.LT_index) != 0:
                self.l_trigger_init = True
        # left pad
        raw_pad = joystick.get_axis(self.LP_index)
        self.state.pad_left = (
            sign(raw_pad) * (abs(raw_pad) ** self.exponent) / 6
        )

    def tick(self):
        """ To be called when a state refresh if required (typically every
            frame)
        """
        # NB: we have to poll the events, otherwise controller state doesn't
        #     get updated. See if we can use this information
        for _ in pygame.event.get():
            pass
        self.update_state()

    def get_state(self):
        """ Get the joystick state as the serialisable JoystickState class

        :returns: JoystickState copy
        :rtype:   JoystickState
        """
        return JoystickState(
            self.state.right_trigger,
            self.state.left_trigger,
            self.state.pad_left,
        )

    def release(self):  # pylint: disable=no-self-use
        """ Release resources held
        """
        pygame.quit()


def main():
    """ Wrapper around the main function for the module
    """
    joystick = PS3Joystick()
    while True:
        try:
            joystick.tick()
            print(joystick.get_state())
            time.sleep(0.01)
        except (KeyboardInterrupt, SystemExit):
            joystick.release()
            print("Exiting")
            break


if __name__ == "__main__":
    main()
