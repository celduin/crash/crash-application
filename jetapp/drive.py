#!/usr/bin/python3
"""This module focuses on making the jetbot move and determine based on a
model if it's OK to move or not"""

import queue
import os
import signal
import sys
import time

import cv2
import pika
import torch
import numpy as np

from jetapp.camera import Camera
from jetapp.image_processing import PreProcessor, ModelsWrapper
from jetapp.robot.robot import Robot
from jetapp.utils import (
    MQConnection,
    get_cli_configs,
    JoystickState,
    LockableStore,
    ensure_data_dir_exists,
    BadImageException,
)


class CarBrain:
    """ Encapsulates the NN models + camera - provides the road following
        behaviour and obstacle avoidance.
    """

    camera_class = Camera
    MQClass = MQConnection

    def __init__(
        self,
        visuals=False,
        host="somehost",
        steering="best_steering_model_xy.pth",
        avoid="best_model.pth",
        count_cat=2,
        go_cat=0,
        stop_cat=1,
        slow_cat=2,
    ):
        self.camera = CarBrain.camera_class(
            stream=visuals, configs={"host": host}
        )

        self.max_allow_time = 0.5

        self.robot = Robot()

        try:
            self.mq_conn = self.MQClass(host, queue_name="jetbot")
        except pika.exceptions.ConnectionBlockedTimeout:
            self.mq_conn = None

        self.visuals = visuals

        self.preproc = PreProcessor(mode=PreProcessor.Mode.EVAL)

        models = ModelsWrapper(steering, avoid, count_cat=count_cat)
        self.model_drive = models.model_drive
        self.model_avoid = models.model_avoid

        self.go_cat = go_cat
        self.stop_cat = stop_cat
        self.slow_cat = slow_cat

    @staticmethod
    def get_time():
        """Get the time so we can check how long things take
        by not calling time directly this lets us customise this function
        more easily. This is over ridden for testing.

        :returns: the current time in seconds
        :rtype: float
        """
        return time.time()

    def tick(self):
        """Process a single frame of video and decide what what to set the
        motors to take based off the frame"""
        frame = self.camera.get_frame()
        start_process_time = self.get_time()

        if frame is None:
            sys.stderr.write("Warning: no frame returned from camera\n")
            return False

        # Get model predictions for road following and obstacle avoid
        #
        # The preprocessor returns a single image, but pytorch wants an ndarray
        # to use in the model, so we reshape it before it is evaluated.
        #
        try:
            radius_recip = float(
                self.model_drive(self.preproc.preprocess(frame))
                .detach()
                .float()
                .cpu()
                .numpy()
                .flatten()
            )
        except BadImageException as bad_image_ex:
            self.robot.stop()
            print(bad_image_ex)
            return True

        avoid_scores = self.model_avoid(self.preproc.preprocess_av(frame))
        _, indices = torch.max(avoid_scores, 1)
        avoid_result = indices[0]

        end_process_time = self.get_time()
        total_process_time = end_process_time - start_process_time
        if total_process_time > self.max_allow_time:
            self.robot.stop()
            return True

        if avoid_result == self.stop_cat:
            self.robot.stop()
            print("blocked")
        else:
            if avoid_result == self.slow_cat:
                self.robot.speed_avg = self.robot.speed_max * 0.5
            else:
                self.robot.speed_avg = self.robot.speed_max
            self.robot.set_turn_radius_recip(radius_recip)
            print("good")

        if abs(self.robot.turn_rad_recip):
            turn_radius = 1 / self.robot.turn_rad_recip
        else:
            turn_radius = np.inf
        print(
            "{} {} :: {} {}".format(
                radius_recip,
                turn_radius,
                self.robot.left_motor.value,
                self.robot.right_motor.value,
            )
        )
        if cv2.waitKey(3) & 0xFF == ord("q"):
            return False
        return True

    def stop(self):
        """ Stops the robot from moving
        """
        self.robot.stop()

    def release(self):
        """Stops the jetbot application from running"""
        print("releasing")
        self.robot.stop()
        if self.mq_conn:
            self.mq_conn.release()
        self.camera.release()


class PilotBrain(CarBrain):
    """ Subclass of the car brain used for manual driving.
        Instead of being driven by a model, we drive via a joystick of some
        description.
    """

    camera_class = Camera

    def __init__(self, visuals=True, host="somehost", data_dir=""):
        # pylint: disable=super-init-not-called
        self.camera = CarBrain.camera_class(
            stream=visuals, configs={"host": host}
        )
        self.robot = Robot()

        self.cmd_queue = LockableStore()
        self.control_data = None
        self.mq_conn = MQConnection(
            host, queue_name="jetbot", local_queue=self.cmd_queue
        )
        self.mq_conn.start_consume()

        self.joystick_state = JoystickState()

        self.data_dir = data_dir
        if self.data_dir:
            ensure_data_dir_exists(self.data_dir)
            self.image_counter = 0
            self.sample_rate = 10
            self.sample_counter = 0

    def tick(self):
        """ Get command and change Robot state to match
        """
        # Poll local_queue for latest control data
        try:
            self.control_data = self.cmd_queue.get()
        except queue.Empty:
            pass
        joystick_state = JoystickState.from_string(self.control_data)

        # Update robot
        if joystick_state is not None:
            self.joystick_state = joystick_state
            self.robot.speed_avg = self.robot.speed_max * (
                joystick_state.right_trigger - joystick_state.left_trigger
            )
            print(joystick_state.pad_left)
            self.robot.set_turn_radius_recip(joystick_state.pad_left)

        # Save training data
        if self.data_dir:
            if self.sample_counter == (self.sample_rate - 1):
                frame = self.camera.get_frame()

                if frame is None:
                    print(
                        "Warning: no frame returned from camera\n",
                        file=sys.stderr,
                    )
                    return False
                filename = "img_{}_R_{}_V_{}~{}.jpg".format(
                    self.image_counter,
                    self.robot.turn_rad_recip,
                    self.robot.left_motor.value,
                    self.robot.right_motor.value,
                )
                self.image_counter += 1
                print("Writing {}".format(filename))
                cv2.imwrite(os.path.join(self.data_dir, filename), frame)
            self.sample_counter = (self.sample_counter + 1) % self.sample_rate

        return True

    def release(self):
        self.stop()
        self.mq_conn.stop_consume()
        self.mq_conn.release()
        self.camera.release()


def cli_add_arguments(parser, defaults):
    """ Func for adding arguments in get_cli_configs()
    :param parser:  contains the previous arguments and used for adding
                    arguments onto
    :type parser:   argparse.ArgumentParser
    :param defaults:contains the information for the IP address and visuals
    :type defaults: dict"""
    parser.add_argument(
        "--host",
        dest="host",
        help="Host to stream data to",
        type=str,
        default=defaults.get("host"),
        nargs="?",
    )

    parser.add_argument(
        "--visuals",
        dest="visuals",
        help="Whether or not to stream video and data for visualisation",
        type=bool,
        default=defaults.get("visuals"),
        nargs="?",
    )

    parser.add_argument(
        "--manual",
        dest="manual",
        help="Piloted by human or not",
        type=bool,
        default=defaults.get("manual"),
        nargs="?",
    )

    parser.add_argument(
        "--data-dir",
        dest="data_dir",
        help="Location to save labelled training data",
        type=str,
        default=defaults.get("data_dir"),
        nargs="?",
    )

    parser.add_argument(
        "--go-cat",
        dest="go_cat",
        help="Index of cat used to indicate the robot should go",
        type=int,
        default=defaults.get("go_cat"),
        nargs="?",
    )

    parser.add_argument(
        "--stop-cat",
        dest="stop_cat",
        help="Index of cat used to indicate the robot should stop",
        type=int,
        default=defaults.get("stop_cat"),
        nargs="?",
    )

    parser.add_argument(
        "--slow-cat",
        dest="slow_cat",
        help="Index of cat used to trigger travelling slowly",
        type=int,
        default=defaults.get("slow_cat"),
        nargs="?",
    )

    parser.add_argument(
        "--count-cat",
        dest="count_cat",
        help="categories of model",
        type=int,
        default=defaults.get("count_cat"),
        nargs="?",
    )


def main():
    """Connects to the jetbot and tries to run the tick"""

    def sigterm_handler(sig, frame):
        # pylint: disable=unused-argument
        # This will cause the except block at the end of main() to run
        # if the preceding try block has been reached, which will ensure
        # the wheels stop when a SIGTERM is received. If the try block
        # isn't reached then we exit without doing anything as the
        # wheels won't be moving anyway
        sys.exit(0)

    signal.signal(signal.SIGTERM, sigterm_handler)

    defaults = {
        "host": "celduin-local-net.guest.codethink.co.uk",
        "visuals": False,
        "manual": False,
        "data_dir": "",
        "go_cat": 0,
        "stop_cat": 1,
        "slow_cat": 2,
        "count_cat": 2,
    }
    configs = get_cli_configs(
        "Car Brain",
        "Road follower and lego avoider",
        defaults,
        cli_add_arguments,
    )
    if configs["manual"]:
        brain = PilotBrain(
            host=configs["host"],
            data_dir=configs["data_dir"],
            visuals=configs["visuals"],
        )
    else:
        brain = CarBrain(
            host=configs["host"],
            visuals=configs["visuals"],
            go_cat=configs["go_cat"],
            stop_cat=configs["stop_cat"],
            slow_cat=configs["slow_cat"],
            count_cat=configs["count_cat"],
        )

    try:
        while True:
            if not brain.tick():
                break
            # TODO: we probably want to sync the ticks with frame rate to
            # avoid

            # polling the camera more frequently than there are frames
            # available
    except (KeyboardInterrupt, SystemExit):
        print("Exiting")
        brain.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
