#!/usr/bin/python3
"""Trains the jetbot to drive and look out for objects"""

import os

import cv2
import torch
import torch.optim as optim
import torch.nn.functional as F
from progress.bar import FillingSquaresBar
import numpy as np

from jetapp.image_processing import (
    new_drive_model,
    new_avoid_model,
    parse_filename,
    PreProcessor,
)
from jetapp.utils import get_cli_configs, get_torch_device


class Basedataset(torch.utils.data.Dataset):
    # pylint: disable=abstract-method
    # This is because Basedataset is also a abstract class
    # TODO make this class explicitly abstract.
    """ Generates a pytorch dataset from a given directory. When generating, it
        also preprocesses the images
    """

    @staticmethod
    def validate_filename(filename):
        """ This function is used to test if a file should be consided
        data. It can be overwriden as subclasses may have diffrent opinios
        but do not need to fully reimplement __init__

        :param filename: the file name to be checked
        :type filename: str

        :returns: wheather or not to use a filename as data
        :rtype: bool
        """
        return (
            parse_filename(filename)[1] != 0
            and os.path.splitext(filename)[1] == ".jpg"
        )

    def __init__(self, directory, random_hflips=False, categories=None):
        self.directory = directory
        self.random_hflips = random_hflips
        self.categories = categories

        self.image_paths = []
        for root, _, files in os.walk(directory, topdown=False):
            for name in files:
                if self.validate_filename(name):
                    self.image_paths.append(os.path.join(root, name))

        self.preprocessor = PreProcessor(mode=PreProcessor.Mode.TRAIN)

    def __len__(self):
        return len(self.image_paths)


class Steerdataset(Basedataset):
    """ pytorch dataset for steering datasets"""

    def __getitem__(self, idx):
        image_path = self.image_paths[idx]

        image = cv2.imread(image_path)
        rad, _ = parse_filename(os.path.basename(image_path))

        # TODO investigate if there is a performance benefit
        # to cacheing this to disk.
        flip = False
        if self.random_hflips and np.random.randint(2):
            rad *= -1
            flip = True
        image = self.preprocessor.preprocess(image, hflip=flip)

        # pylint: disable=not-callable
        return image, torch.tensor([rad]).float()


class Obstacledataset(Basedataset):
    """ pytorch dataset for obstacle datasets"""

    def validate_filename(self, filename):
        """ This function is used to test if a file should be consided
        data. This subclass overwrite the inherited function.
        It can be futher overwriden as subclasses may have diffrent opinios.

        :param filename: The file name to be checked
        :type filename: str

        :returns: Wheather or not to use a filename as data
        :rtype: bool
        """
        return os.path.splitext(filename)[1] == ".jpg"

    def cat_from_filename(self, filename):
        """ work out the index of a datapoints category form its filename

        :param filename: The file name to be checked
        :type filename: str

        :returns: Index of category
        :rtype: int

        """
        category = 0
        for enum_index, cat in enumerate(self.categories):
            if cat in filename:
                category = enum_index + 1
        return category

    def __getitem__(self, idx):
        image_path = self.image_paths[idx]

        image = cv2.imread(image_path)
        category = self.cat_from_filename(image_path)

        # TODO investigate if there is a performance benefit
        # to cacheing this to disk.
        image = self.preprocessor.preprocess_av(image)

        # pylint: disable=not-callable
        return image, torch.tensor(category).long()


def iterate(
    model, device, train_loader, test_loader, optimizer, loss_function
):
    """ Runs a single epoch of training.

        In more detail, this trains with feed-forward + back propagation on
        each image in the train_loader, and then checks the error on each image
        in the test_loader.

        :param model:           The model you want to train
        :type model:            torch.nn.Module

        :param device:          Which torch device to use
        :type device:           torch.device

        :param train_loader:    The training dataset
        :type train_loader:     torch.utils.data.DataLoader

        :param test_loader:     The testing dataset
        :type test_loader:      torch.utils.data.DataLoader

        :param optimizer:       Optimizer to be used
        :type optimizer:        torch.optim.Optimizer

        :param loss_function:    Loss function to be used
        :type loss_function:     func

        :returns:               A tuple containing the train loss and test loss
        :rtype:                 (float, float)
    """

    model.train()
    train_loss = 0.0
    train_bar = FillingSquaresBar("Training", max=len(train_loader))
    for images, labels in iter(train_loader):
        images = images.to(device)
        labels = labels.to(device)
        optimizer.zero_grad()
        outputs = model(images)
        loss = loss_function(outputs, labels)
        train_loss += loss
        loss.backward()
        optimizer.step()
        train_bar.next()
    train_loss /= len(train_loader)
    train_bar.finish()

    if device == torch.device("cuda"):
        torch.cuda.empty_cache()

    model.eval()
    test_loss = 0.0
    test_bar = FillingSquaresBar("Testing", max=len(test_loader))
    with torch.no_grad():
        for images, labels in iter(test_loader):
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            loss = loss_function(outputs, labels)
            test_loss += loss
            test_bar.next()
    test_loss /= len(test_loader)
    test_bar.finish()

    if device == torch.device("cuda"):
        torch.cuda.empty_cache()

    return train_loss, test_loss


def train_steer(datalocation, outputfile, test_percent=0.1, epochs=70):
    """ Train a fresh model on a dataset.

        :param datalocation:    Directory for the dataset
        :type datalocation:     str

        :param outputfile:      Where to save the trained model
        :type outputfile:       str

        :param test_percent:    proportion of the data set that will be used to
                                test the model's accuracy
        :type test_percent:     float

        :param epochs: number of times to iterate
        :type epochs:  int
    """

    dataset = Steerdataset(datalocation, random_hflips=True)

    num_test = int(test_percent * len(dataset))
    train_dataset, test_dataset = torch.utils.data.random_split(
        dataset, [len(dataset) - num_test, num_test]
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=1, shuffle=True, num_workers=1
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=1, shuffle=True, num_workers=1
    )

    device = get_torch_device()

    model = new_drive_model(pretrained=True, device=device)

    optimizer = optim.Adam(model.parameters())

    best_loss = 1e9
    for epoch in range(epochs):
        train_loss, test_loss = iterate(
            model, device, train_loader, test_loader, optimizer, F.mse_loss
        )
        print("Epoch {} of {}".format(epoch + 1, epochs))
        print("%d: %f, %f" % (epoch + 1, train_loss, test_loss))
        if test_loss < best_loss:
            torch.save(model.state_dict(), outputfile)
            best_loss = test_loss


def train_avoid(
    datalocation, outputfile, test_percent=0.1, epochs=70, categories=None
):
    """ Train a fresh model on a dataset.
    """

    if categories is None:
        categories = []

    dataset = Obstacledataset(datalocation, categories=categories)

    num_test = int(test_percent * len(dataset))
    train_dataset, test_dataset = torch.utils.data.random_split(
        dataset, [len(dataset) - num_test, num_test]
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=1, shuffle=True, num_workers=1
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=1, shuffle=True, num_workers=1
    )

    device = get_torch_device()

    model = new_avoid_model(
        pretrained=True, device=device, categories=len(categories) + 1
    )

    # optimizer = optim.Adam(model.parameters())
    optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

    best_loss = 1e9
    for epoch in range(epochs):
        train_loss, test_loss = iterate(
            model,
            device,
            train_loader,
            test_loader,
            optimizer,
            F.cross_entropy,
        )
        print("Epoch {} of {}".format(epoch + 1, epochs))
        print("%d: %f, %f" % (epoch + 1, train_loss, test_loss))
        if test_loss < best_loss:
            torch.save(model.state_dict(), outputfile)
            best_loss = test_loss


def cli_add_arguments(parser, defaults):
    """ Func for adding arguments in get_cli_configs()
    """
    parser.add_argument(
        "--datadir",
        dest="datadir",
        help="Directory training data is stored in",
        type=str,
        default=defaults.get("datadir", "training"),
        nargs="?",
    )

    parser.add_argument(
        "--category",
        action="append",
        dest="categories",
        default=[],
        type=str,
        help="Add repeated values to a list",
    )

    parser.add_argument(
        "--modelfile",
        dest="modelfile",
        help="Name of model file to output to",
        type=str,
        default=defaults.get("modelfile", "model.pth"),
        nargs="?",
    )

    parser.add_argument(
        "--test-percent",
        dest="testpercent",
        help="Percentage of training data to use for validation",
        type=float,
        default=defaults.get("testpercent", 0.2),
        nargs="?",
    )

    parser.add_argument(
        "--epochs",
        dest="epochs",
        help="Number of epochs to run for",
        type=int,
        default=defaults.get("epochs", 70),
        nargs="?",
    )


def main():
    """ Runs train_r with dataset_xy and best_steering_model_r.pth as
        params
    """
    defaults = {
        "datadir": "dataset_xy",
        "modelfile": "best_steering_model_r.pth",
        "testpercent": 0.2,
        "epochs": 70,
    }
    configs = get_cli_configs(
        "Model Trainer",
        "Trains models for steering",
        defaults,
        cli_add_arguments,
    )
    if len(configs["categories"]) > 0:
        train_avoid(
            datalocation=configs["datadir"],
            outputfile=configs["modelfile"],
            test_percent=configs["testpercent"],
            epochs=configs["epochs"],
            categories=configs["categories"],
        )
    else:
        train_steer(
            datalocation=configs["datadir"],
            outputfile=configs["modelfile"],
            test_percent=configs["testpercent"],
            epochs=configs["epochs"],
        )


if __name__ == "__main__":
    main()
