Overview
========

This directory contains all of the files used for training models, capturing
data and driving the jetbot

# Joystick

The joystick.py file contains classes for reading input from a PS3 controller.
These classes have been written generally, and the JoystickState (in utils.py)
could be implemented for any controller type.

Setup
=====

# Joystick

In order to use the PS3 controller; you have to install the following software:
```
git clone https://github.com/falkTX/qtsixa
cd qtsixa
make
sudo make install
sudo sixad -s
```
Note that it comes with a GUI to aid in the setup (`qtsixa`) and that you have
to restore the normal bluetooth behaviour of your machine by running:
```
sudo sixad -r
```