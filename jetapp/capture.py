#!/usr/bin/python3
"""Used for capturing data, highlight important parts from the camera and
saving it"""

import copy
import os
from uuid import uuid1

import cv2

from jetapp.camera import UdpSrc
from jetapp.utils import ensure_data_dir_exists

DATASET_DIR = "dataset_xy"


def bgr8_to_jpeg(value):
    """Converts the frame to a jpeg image

    :param value:   frame of the video from OpenCV
    :type value:    numpy.array

    :returns:   jpeg of the value
    :rtype:     bytes
    """

    return bytes(cv2.imencode(".jpg", value)[1])


class ImageLabeller:
    """ Uses opencv gui to enable manual labelling of images
    """

    def __init__(self):
        self.receiver = UdpSrc()

        self.last_frame = self.receiver.last_frame

        self.coord_x = None
        self.coord_y = None

        cv2.imshow("jetbot-vision", self.last_frame)
        cv2.setMouseCallback("jetbot-vision", self.draw_circle)

    def draw_circle(self, event, coord_x, coord_y):
        """Draws a circle at coordinates (x, y)
        :param event:           check if the left or right wouse button is
                                being pressed
        :type event:            int

        :param coord_x:         X coordinate of the center of the circle
        :type coord_x:          int

        :param coord_y:         Y coordinate of the center of the circle
        :type coord_y:          int

        :raises RuntimeError:   if no frame is able to be captured
        """

        if event == cv2.EVENT_RBUTTONDOWN:
            self.last_frame = self.receiver.get_frame()
            if self.last_frame is None:
                raise RuntimeError("Error: no frame returned from camera")
            cv2.imshow("jetbot-vision", self.last_frame)
        elif event == cv2.EVENT_LBUTTONDOWN:
            withcir = copy.copy(self.last_frame)
            print(coord_x, coord_y)
            cv2.circle(withcir, (coord_x, coord_y), 5, (0, 255, 0), 1)
            self.coord_x = (coord_x / 224 - 0.5) * 2
            self.coord_y = (coord_y / 224 - 0.5) * 2

            cv2.imshow("jetbot-vision", withcir)  # to display the characters

    def save_image(self):
        """Saves the current frame as an image at the current directory"""
        uuid = "xy_%03d_%03d_%s" % (
            self.coord_x * 50 + 50,
            self.coord_y * 50 + 50,
            uuid1(),
        )
        image_path = os.path.join(DATASET_DIR, uuid + ".jpg")
        jpeg_image = bgr8_to_jpeg(self.last_frame)
        with open(image_path, "wb") as image_path_file:
            image_path_file.write(jpeg_image)


def main():
    """Cheks if we can and if so save an image"""
    ensure_data_dir_exists(DATASET_DIR)

    labeller = ImageLabeller()
    while True:

        key = cv2.waitKey(1)
        if key == ord("s"):
            print("we should save the thing")
            labeller.save_image()
        if key == 27 or key == ord("q"):
            break
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
