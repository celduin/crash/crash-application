#!/usr/bin/python3
"""Used for interfacing with the camera"""

import atexit
import sys

import cv2


class Camera:
    """ Encapsulates the opencv camera access
    """

    # TODO: make the following configurable
    defaults = {
        "width": 224,
        "height": 224,
        "fps": 21,
        "capture_width": 1920,
        "capture_height": 1080,
        "host": "172.16.100.92",
        "rtp_port": 5000,
        "rtcp_port": 5001,
    }

    def __init__(self, stream=False, configs=None):
        """ Initialise gst pipeline and grab 1st frame

            An example RX pipeline would be:
            gst-launch-1.0 udpsrc port={rtp_port} ! \
                application/x-rtp,encoding-name=H264,payload=96 ! \
                rtph264depay ! h264parse ! queue ! avdec_h264 ! \
                xvimagesink sync=false async=false -e
        """
        self.capture_width = self.defaults["capture_width"]
        self.capture_height = self.defaults["capture_height"]
        self.fps = self.defaults["fps"]
        self.width = self.defaults["width"]
        self.height = self.defaults["height"]
        self.host = self.defaults["host"]
        self.rtp_port = self.defaults["rtp_port"]
        self.rtcp_port = self.defaults["rtcp_port"]

        self._set_configs(configs)

        print(
            "opening gstreamer pipeline:\n",
            self._gstreamer_str(stream),
            "\n\n",
        )
        self.cap = cv2.VideoCapture(
            self._gstreamer_str(stream), cv2.CAP_GSTREAMER
        )

        if not self.cap.isOpened():
            raise RuntimeError("Videocap not opened\n")

        ret_val, frame = self.cap.read()

        if not ret_val:
            raise RuntimeError("Could not read image from camera.")

        self.last_frame = frame
        atexit.register(self.release)

    def _set_configs(self, configs):
        """Sets the Camera attributes from the params dict representing the
        configuration
        :param configs: the configurations you want to set
        :type configs:  dict
        """
        if configs is None:
            configs = self.defaults

        for config in configs.keys():
            if config in self.defaults:
                setattr(self, config, configs[config])
            else:
                sys.stderr.write(
                    "Warning: Invalid Camera key passed {}\n".format(config)
                )

    def _gstreamer_str(self, stream):
        """ Get gst-launch-1.0 command for opencv to run
        :param stream:  indicates if we should stream in data through
                        gstreamer
        :type stream:   bool

        :returns:       string for opencv to run as gstreamer pipeline
        :rtype:         str
        """

        # TODO: extend this to use rtpbin
        # TODO: we could have subclasses that reimplement this for testing
        #       purposes
        if stream:
            stream_pipeline = (
                "tee name=t t. ! queue ! nvv4l2h264enc bitrate=8000000 "
                "insert-sps-pps=true ! rtph264pay mtu=1400 ! "
                "udpsink host={0} port={1} sync=false async=false "
                "t. ! queue! "
            ).format(self.host, self.rtp_port)
        else:
            stream_pipeline = ""

        pipeline = (
            "nvarguscamerasrc ! "
            "video/x-raw(memory:NVMM), width=(int){0}, height=(int){1}, "
            "framerate=(fraction){2}/1 ! {5}nvvidconv ! video/x-raw, "
            "width=(int){3}, height=(int){4}, format=(string)BGRx ! "
            "videoconvert ! video/x-raw, format=(string)BGR ! "
            "appsink drop=true max-buffers=2"
        )
        return pipeline.format(
            self.capture_width,
            self.capture_height,
            self.fps,
            self.width,
            self.height,
            stream_pipeline,
        )

    def get_frame(self):
        """ Polls opencv for the latest frame

        :returns:   numpy array of shape (width, height, 3)
        :rtype:     numpy.array
        """
        ret_val, frame = self.cap.read()

        self.last_frame = frame
        if not ret_val:
            return None
        return frame

    def get_dimensions(self):
        """ Returns (width, height) tuple
        """
        width = self.cap.get(3)
        height = self.cap.get(4)
        return (width, height)

    def release(self):
        """ Release camera resources on python interpreter termination
        """
        # TODO: work out why releasing the capture results in an intermittent
        # freeze bug

        # pass
        self.cap.release()
        if self.cap.isOpened():
            self.cap.release()


class UdpSrc(Camera):
    """Inherits from Camera, but to be used on the client to display camera
    data by pointing at the UDP/RTP stream"""

    def __init__(self, configs=None):
        super().__init__(stream=False, configs=configs)

    def _gstreamer_str(self, _):
        pipeline = (
            "udpsrc port={0} ! application/x-rtp,media=(string)video,"
            "clock-rate=(int)90000,encoding-name=(string)H264,"
            "payload=(int)96 ! rtph264depay ! decodebin ! videoconvert ! "
            "video/x-raw, format=(string)BGR ! appsink"
        )

        return pipeline.format(self.rtp_port)
