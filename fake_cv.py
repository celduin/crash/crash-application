#!/usr/bin/python
"""This script a link to OpenCV into venvs so it can be used on the Nvidia
Jetson"""

import subprocess
import os
import platform
import argparse


def addlink(root=".tox/py36"):
    """Checks if it's on the arch64 platform and links to it
    :param root: the path to the tox venv
    :type root:  string"""
    newfile = (
        "{root}/lib/python3.6/cv2.cpython-36m-aarch64-linux-gnu.so"
    ).format(root=root)

    if platform.machine() == "aarch64" and not os.path.isfile(newfile):
        subprocess.call(
            [
                "ln",
                "-s",
                "/usr/lib/python3.6/dist-packages/"
                "cv2.cpython-36m-aarch64-linux-gnu.so",
                newfile,
            ]
        )


def main():
    """This the function that the package uses to set up a script entry point
    The function parses the cli options and calls the rest of the script"""
    parser = argparse.ArgumentParser(
        description="Fixes venvs for Nvidia python3-openCV"
    )
    parser.add_argument("--newroot", default=".tox/py36", help="root")
    parser.add_argument(
        "--force", default=".tox/py36", help="always make link"
    )
    args = parser.parse_args()
    if "JETAPP_FAKECV" in os.environ or args.force:
        addlink(args.newroot)


if __name__ == "__main__":
    main()
