JetApp
======

This is a basic application for using the jetbot robot without needing jupyter notebooks.

Dependencies
------------

Unfortunately the `opencv-python` package from PyPI does not have gstreamer support, which is needed
to run the driving on the jetbot and the client application. As a result it is omitted from the
requirements files, as installing it here breaks existing versions from distributions.

Even more problematically some distributions don't compile opencv with gstreamer support, to check if
your openCV has such support run

```python
>>> import cv2
>>> print(cv2.getBuildInformation())
```

and check the GStreamer option. If this is NO, you may need to compile opencv from source.

Testing
=======

We have unit tests in the pytest frame work, this may be expanded to some intergration tests in time

We have the black and pylint linters set up

These can all be run through tox but tox is not used in the CI currently however this should be fixed.


Running the tests
-----------------

You can run all the tests with tox by just running 

```
tox
```

This will include the linters and the unit tests, you can then select the 
group of tests `-e pylint`

You can just run one file of tests or just one test by giving its name after `--` eg.

```
tox -e py36 -- test_training.py::test_autogen
```

### Running the tests on a nano

The pip version of openCV is not avaliable for aarch64 in pypi, so it is not in the requirements files as this can cause issues.

If you want to use the nvidia opencv it will not be linked as it is installed in to a odd place. A helper script can be used though.

```
export JETAPP_NOCV=''
export JETAPP_FAKECV=''
tox -e py36
```

If this is not the first time running tox you may want to use ```tox -e py36 -r``` to rebuild the enviroment.

once the environment vars have been set you will not need to reset them until you open a new terminal.


Docker
======

Building
--------

There are docker images available at [the CRASH dockerfile repo.](https://gitlab.com/celduin/crash/docker-images)

See the [README](https://gitlab.com/celduin/crash/docker-images/-/blob/master/README) there for more information.


Running the camera in Docker
----------------------------

First enable X forwarding for root
sudo xhost +si:localuser:root

To run the docker image:

```
sudo docker run --runtime nvidia --network host -it -e DISPLAY=$DISPLAY --privileged -v /tmp/.X11-unix/:/tmp/.X11-unix jetapp:latest
```

To use the training data the nvidia docker run time must be spesified with `--runtime nvidia` as for using the camera but the other options like `--privilaged` or passing throught X is not.
