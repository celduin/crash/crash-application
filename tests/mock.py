"""Contains the mock objects for testing for use without the jetbot"""

import os

import cv2


class MockAdafruit_MotorHAT_motor:
    """Mock object for the Adafruit_MotorHAT_motor"""

    def __init__(self, channel):
        self.channel = channel
        # This tries to mirror
        # https://github.com/adafruit/Adafruit-Motor-HAT-Python-Library/blob/
        # master/Adafruit_MotorHAT/Adafruit_MotorHAT_Motors.py#L249
        assert channel in [0, 1, 2, 3]

        self._call_run = []
        self._call_set_speed = []

    # Needed as the origital Adafruit doesn't follow snake_case convention
    # pylint: disable=invalid-name
    def setSpeed(self, speed):
        """Mimics the original setSpeed, stores and runs the desired RPM on
        the motors
        :param speed:   the speed you want to set
        :type speed:    int"""
        self._call_set_speed.append(speed)

    def run(self, action):
        """Mimics the original run, having 1 of 3 actions
        forward
        backward
        release
        :param action:  the action you want to perform
        :type action:   int"""
        self._call_run.append(action)


class MockAdafruit_MotorHAT:
    """Mock object for the Adafruit_MotorHAT"""

    def __init__(self, i2c_bus):
        ## The real mottor driver is always on bus 1 this could be more
        ## complex

        ## if this changes
        assert i2c_bus == 1, "Wrong i2c bus selected"

    # pylint: disable=invalid-name
    @staticmethod
    def getMotor(index):
        """Mimics the original getMotor, returns the motor at ID 1 to 4"""
        # This tries to mirror https://github.com/adafruit/
        # Adafruit-Motor-HAT-Python-Library/blob/master/Adafruit_MotorHAT/
        # Adafruit_MotorHAT_Motors.py#L249
        assert index in [1, 2, 3, 4]
        return MockAdafruit_MotorHAT_motor(index - 1)


class SimpleCameraMock:
    """Mock object for the camera"""

    mock_image = (
        "img_91_R_-0.10215093288570642~-0.045732101055352685_"
        "V_0.2858481184579432~14.65842484950382.jpg"
    )

    # pylint: disable=unused-argument
    # These arguments are for api compatibleity
    # TODO check stream and configs are valid.
    def __init__(self, stream=None, configs=None):
        pathname = os.path.join(
            os.path.dirname(__file__), "dataset_train_test", self.mock_image
        )
        self.image = cv2.imread(pathname)

    def get_frame(self):
        """Returns the last image captured"""
        return self.image

    def get_dimensions(self):
        """Returns the dimensions of the captured image"""
        height, width, _ = self.image.shape
        return (width, height)


class MQRabbitMock:
    """Mock object for connection"""

    def __init__(self, host, queue_name=None):
        pass
