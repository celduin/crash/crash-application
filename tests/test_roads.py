"""Tests the jetbot driving features on the road"""
import os
import time

import cv2
import torch
from Adafruit_MotorHAT import Adafruit_MotorHAT

from jetapp.image_processing import new_drive_model, new_avoid_model
from jetapp.utils import get_torch_device
from . import mock

DATA_DIR = os.path.dirname(os.path.realpath(__file__))


def setup_carbrain(tmpdir):
    """helper function to create models"""
    from jetapp.robot.robot import Robot
    from jetapp.drive import CarBrain

    outputfile_drive = str(tmpdir.join("test_model_D.pth"))
    outputfile_avoid = str(tmpdir.join("test_model_A.pth"))

    device = get_torch_device()

    model_drive = new_drive_model(pretrained=True, device=device)
    model_avoid = new_avoid_model(pretrained=True, device=device)

    torch.save(model_drive.state_dict(), outputfile_drive)
    torch.save(model_avoid.state_dict(), outputfile_avoid)

    Robot.driver_initialiser = mock.MockAdafruit_MotorHAT
    CarBrain.MQClass = mock.MQRabbitMock
    CarBrain.camera_class = mock.SimpleCameraMock

    car_brain = CarBrain(
        visuals=False, steering=outputfile_drive, avoid=outputfile_avoid
    )

    return car_brain


def test_import_road():
    """This test checks that the module can be imported

    This should be a given but it was not to start with.
    """
    # pylint: disable=unused-import
    from jetapp import drive


def test_follower_object(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    car_brain.tick()

    # pylint: disable=protected-access
    assert len(car_brain.robot.left_motor.motor._call_run) == 1


def test_follower_object_timed(tmpdir):
    """Tests that the CarBrain comminicates with the model in time
    """

    x_second_requirement = 0.5

    car_brain = setup_carbrain(tmpdir)

    start_time = time.time()
    car_brain.tick()
    end_time = time.time()

    device = get_torch_device()
    if device.type != "cpu":
        assert end_time - start_time < x_second_requirement

    # pylint: disable=protected-access
    assert len(car_brain.robot.left_motor.motor._call_run) == 1


def test_correct_ai_turn_right(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to move
    # pylint: disable=unused-argument
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.8, 0.2]])

    car_brain.model_avoid = mock_avoid

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to turn right
    # pylint: disable=unused-argument
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.1838]])

    car_brain.model_drive = mock_drive

    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.left_motor.motor._call_set_speed[-1]
        > car_brain.robot.right_motor.motor._call_set_speed[-1]
    )


def test_correct_ai_turn_left(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to move
    # pylint: disable=unused-argument
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.8, 0.2]])

    car_brain.model_avoid = mock_avoid

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to turn left
    # pylint: disable=unused-argument,function-redefined
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[-0.1838]])

    car_brain.model_drive = mock_drive

    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.left_motor.motor._call_set_speed[-1]
        < car_brain.robot.right_motor.motor._call_set_speed[-1]
    )


def test_correct_ai_straight(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to move
    # pylint: disable=unused-argument
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.8, 0.2]])

    car_brain.model_avoid = mock_avoid

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to travel straight
    # pylint: disable=unused-argument,function-redefined
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.0]])

    car_brain.model_drive = mock_drive

    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.left_motor.motor._call_set_speed[-1]
        == car_brain.robot.right_motor.motor._call_set_speed[-1]
    )


def test_stop_timeout(tmpdir):
    """Test that the robot stops if the processing takes too long
    """

    car_brain = setup_carbrain(tmpdir)

    def mock_time_gen():
        for newtime in [1000, 1001]:
            yield newtime

    time_gen = mock_time_gen()

    def mock_time():
        return time_gen.__next__()

    car_brain.get_time = mock_time

    car_brain.tick()

    # pylint: disable=protected-access
    assert len(car_brain.robot.left_motor.motor._call_run) == 1
    assert car_brain.robot.left_motor.motor._call_set_speed == [0]
    assert car_brain.robot.right_motor.motor._call_set_speed == [0]


def test_badimage_detection(tmpdir):
    """Tests that if an image is too low quality the motors stop
    """

    car_brain = setup_carbrain(tmpdir)

    car_brain.camera.image = cv2.imread(
        str(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)), "blackpicture.jpg"
            )
        )
    )

    car_brain.tick()

    # pylint: disable=protected-access
    assert len(car_brain.robot.left_motor.motor._call_run) == 1
    assert car_brain.robot.left_motor.motor._call_set_speed == [0]
    assert car_brain.robot.right_motor.motor._call_set_speed == [0]


def test_no_images_stop(tmpdir):
    """ Test that whenever the Python script stops receiving images,
    the motors stop"""

    car_brain = setup_carbrain(tmpdir)

    def mock_no_image():
        return None

    car_brain.camera.get_frame = mock_no_image

    car_brain.camera.get_frame()
    car_brain.tick()

    # pylint: disable=protected-access
    assert len(car_brain.robot.left_motor.motor._call_run) == 0
    assert len(car_brain.robot.left_motor.motor._call_set_speed) == 0
    assert len(car_brain.robot.right_motor.motor._call_set_speed) == 0


def test_correct_ai_stop(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to turn right
    # pylint: disable=unused-argument
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.1838]])

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to stop
    # pylint: disable=unused-argument,function-redefined
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.2, 0.8]])

    car_brain.model_drive = mock_drive
    car_brain.model_avoid = mock_avoid
    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert car_brain.robot.left_motor.motor._call_set_speed[-1] == 0
    assert car_brain.robot.right_motor.motor._call_set_speed[-1] == 0


def test_correct_ai_stop_three(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to turn right
    # pylint: disable=unused-argument
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.1838]])

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to stop
    # pylint: disable=unused-argument,function-redefined
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.2, 0.8, -3]])

    car_brain.model_drive = mock_drive
    car_brain.model_avoid = mock_avoid
    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert car_brain.robot.left_motor.motor._call_set_speed[-1] == 0
    assert car_brain.robot.right_motor.motor._call_set_speed[-1] == 0


def test_correct_ai_slow_three(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """
    car_brain = setup_carbrain(tmpdir)

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to turn right
    # pylint: disable=unused-argument
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.0]])

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to stop
    # pylint: disable=unused-argument,function-redefined
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.2, 0.8, 3]])

    car_brain.model_drive = mock_drive
    car_brain.model_avoid = mock_avoid
    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert car_brain.robot.left_motor.motor._call_set_speed[-1] == 48
    assert car_brain.robot.right_motor.motor._call_set_speed[-1] == 48


def test_correct_ai_full_three(tmpdir):
    """Tests that the CarBrain comminicates with the model
    """

    car_brain = setup_carbrain(tmpdir)

    # This defines a mock drive model that will provide
    # a tensor that will trigger the robot to turn right
    # pylint: disable=unused-argument
    def mock_drive(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[0.0]])

    # This defines a mock avoid model that will provide
    # a tensor that will trigger the robot to stop
    # pylint: disable=unused-argument,function-redefined
    def mock_avoid(processed_image):
        # pylint: disable=not-callable
        return torch.tensor([[4, 0.8, 3]])

    car_brain.model_drive = mock_drive
    car_brain.model_avoid = mock_avoid
    car_brain.tick()

    # pylint: disable=protected-access
    assert (
        car_brain.robot.left_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert (
        car_brain.robot.right_motor.motor._call_run[-1]
        == Adafruit_MotorHAT.BACKWARD
    )
    assert car_brain.robot.left_motor.motor._call_set_speed[-1] == 76
    assert car_brain.robot.right_motor.motor._call_set_speed[-1] == 76
