"""Tests if the robot object will work"""

from Adafruit_MotorHAT import Adafruit_MotorHAT

from . import mock


def test_import_robot():
    """This test checks that the module can be imported

    This should be a given but it was not to start with.
    """
    # pylint: disable=unused-import
    from jetapp import robot
    from jetapp.robot.robot import Robot


def test_robot_init():
    """Tests that the robot is able it initalise"""
    from jetapp.robot.robot import Robot

    Robot.driver_initialiser = mock.MockAdafruit_MotorHAT

    robot = Robot()

    assert robot.left_motor.motor.channel == 0
    assert robot.right_motor.motor.channel == 1


def test_robot_halfpower():
    """Tests that the robot runs correctly at half power"""
    from jetapp.robot.robot import Robot

    Robot.driver_initialiser = mock.MockAdafruit_MotorHAT

    robot = Robot()

    robot.set_motors(0.5, 0.5)

    # pylint: disable=protected-access
    assert len(robot.left_motor.motor._call_run) == 1
    assert len(robot.right_motor.motor._call_run) == 1

    assert Adafruit_MotorHAT.BACKWARD in robot.left_motor.motor._call_run
    assert Adafruit_MotorHAT.BACKWARD in robot.right_motor.motor._call_run
