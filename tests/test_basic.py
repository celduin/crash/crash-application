"""Tests basic parts of the application like importing"""


def test_import_capture():
    """Tests if the capture module can be imported"""
    # pylint: disable=unused-import
    from jetapp import capture
