"""Used for training the jetbot so it can improve"""

import os

from PIL import Image
import numpy as np
import cv2

DATA_DIR = os.path.dirname(os.path.realpath(__file__))


def test_import_train():
    """This test checks that the module can be imported

    This should be a given but it was not to start with.
    """
    # pylint: disable=unused-import
    from jetapp import train


def test_full_train_steer(tmpdir):
    """Run a full training cycle for steer
    """
    from jetapp import train

    data_folder = os.path.join(DATA_DIR, "dataset_train_test")
    outputfile = str(tmpdir.join("test_model_steer.pth"))
    train.train_steer(data_folder, outputfile, epochs=2)

    assert os.path.isfile(
        outputfile
    ), "This test should have created: {}".format(outputfile)


def test_full_train_avoid(tmpdir):
    """Run a full training cycle for avoid
    """
    from jetapp import train

    data_folder = os.path.join(DATA_DIR, "dataset_train_avoid_test")
    outputfile = str(tmpdir.join("test_model_avoid.pth"))
    train.train_avoid(
        data_folder,
        outputfile,
        epochs=2,
        test_percent=0.5,
        categories=["catb"],
    )

    assert os.path.isfile(
        outputfile
    ), "This test should have created: {}".format(outputfile)


def test_full_train_avoid_three(tmpdir):
    """Run a full training cycle for avoid
    """
    from jetapp import train

    data_folder = os.path.join(DATA_DIR, "dataset_train_avoid_test")
    outputfile = str(tmpdir.join("test_model_avoid.pth"))
    train.train_avoid(
        data_folder,
        outputfile,
        epochs=2,
        test_percent=0.5,
        categories=["catb", "catc"],
    )

    assert os.path.isfile(
        outputfile
    ), "This test should have created: {}".format(outputfile)


def create_training_image(ang, filename):
    """Creates a spoof training image to use in testing the jetbot"""

    img = np.zeros((224, 224, 3), np.uint8)
    img[:, :, :] = 255
    ang = ang / 180 * np.pi
    bottom = np.array((112, 0))
    top = bottom + np.array((50 * np.sin(ang), 50 * np.cos(ang)))
    cv2.line(
        img,
        tuple(bottom.astype(np.int)),
        tuple(top.astype(np.int)),
        (255, 0, 0),
        5,
    )
    # Assuming v=1, radius reciprocal is tan(pi/2 - ang)
    rad = np.tan((np.pi / 2) - ang)
    img = Image.fromarray(img)
    head, tail = os.path.split(filename)
    img.save(
        str(
            os.path.join(
                head,
                "{}_R_{}~{}_V_{}~{}.jpg".format(
                    tail, rad.astype(np.int), rad.astype(np.int), 1.0, 1.0
                ),
            )
        )
    )
    return top


def test_autogen(tmpdir):
    """create anilical data and use it to train
    """
    from jetapp import train

    dataset_location = tmpdir.join("dataset_auto_gen")
    os.makedirs(dataset_location)
    for img_id, ang in enumerate(np.linspace(-20, 20, 30)):
        create_training_image(
            ang, dataset_location.join("img_{}".format(img_id))
        )

    outputfile = str(tmpdir.join("test_model.pth"))
    train.train_steer(dataset_location, outputfile, epochs=2)

    assert os.path.isfile(
        outputfile
    ), "This test should have created: {}".format(outputfile)
