"""Tests the networking for the jetbot-application work"""

import queue

import pytest

from jetapp.camera import Camera, UdpSrc
from jetapp.utils import Circle, MQConnection


def test_circle_serialisation():
    """ Tests the serialisation of a circle
    """
    circle_input = Circle((100, 200), 20)
    circle_str = circle_input.to_string()
    circle_output = Circle.from_string(circle_str)

    assert circle_input == circle_output


@pytest.mark.xfail(strict=True)
def test_mq_connection():
    """ Basic test of message flow across RabbitMQ MQ

    TODO: work out how to setup RabbitMQ environment in pytest
          or maybe this test should only be run on the nano?
    """
    local_queue = queue.Queue()
    mq_conn = MQConnection(
        "localhost", user="guest", password="guest", local_queue=local_queue
    )

    mq_conn.start_consume()

    text_payload = "this is some text that we want to send over MQ"
    mq_conn.send(text_payload)

    try:
        queue_result = local_queue.get(timeout=3)
    except queue.Empty:
        assert False

    assert queue_result == text_payload


@pytest.mark.xfail(strict=True)
def test_camera_streaming():
    """ Initialises sensor based Camera and points UdpSrc at it, to
        see if frames can be retrieved
    """
    camera = Camera(stream=True, configs={"host": "localhost"})
    receiver = UdpSrc(configs={"host", "localhost"})

    frame = receiver.get_frame() is not None

    camera.release()
    receiver.release()

    assert frame is not None
